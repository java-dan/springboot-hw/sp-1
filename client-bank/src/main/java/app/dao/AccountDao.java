package app.dao;

import app.entity.Account;
import app.entity.Currency;
import app.entity.Customer;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class AccountDao implements DAO<Account>{
    private final List<Account> data = new ArrayList<>();
    private static long count = 1;

    public AccountDao() {
        fill();
    }

    private void fill() {
        Customer elena = new Customer("Elena", "elena@gmail.com", 27);
        elena.setId(1L);
        Customer max = new Customer( "Max", "max@gmail.com", 55);
        max.setId(2L);
        Customer gim = new Customer("Gim", "gim@gmail.com", 38);
        gim.setId(3L);
        Account elenaA = new Account(null,"f2962718-8cf6-5040-b44d-f62c042f5d41",Currency.USD, 4000.00, elena);
        Account elenaA2 = new Account(null,"96ad01f4-332b-59b9-bb72-131305c5636a", Currency.UAH, 37.80, elena);
        Account maxA = new Account(null, "8e39f30d-6402-5d5d-85ac-86ee78b423cf",  Currency.EUR, 250.00, max);
        Account gimA = new Account(null,"c359657d-6995-5c3d-b85d-84871637954c", Currency.USD, 700.00, gim);
        save(elenaA);
        save(elenaA2);
        save(maxA);
        save(gimA);
    }

    @Override
    public Account save(Account obj) {
        if(!data.contains(obj)) {
            obj.setId(count++);
            data.add(obj);
        }
//        else {
//            int i = data.indexOf(obj);
//            data.set(i, obj);
//        }
        return obj;
    }

    @Override
    public boolean delete(Account obj) { return data.remove(obj); }

    @Override
    public void deleteAll(List<Account> entities) { data.removeAll(entities); }

    @Override
    public void saveAll(List<Account> entities) { data.addAll(entities); }

    @Override
    public List<Account> findAll() { return data; }

    @Override
    public boolean deleteById(long id) { return data.removeIf(a -> a.getId() == id); }

    @Override
    public Account getOne(long id) {
        return data
                .stream()
                .filter(a -> Objects.equals(a.getId(), id))
                .findFirst()
                .orElse(null);
    }

    public Account createAccount(Currency currency, Customer customer) {
        Account account = new Account(currency, customer);
        return save(account);
    }

    public Account findByNumber(String number) {
        return data
                .stream()
                .filter(n -> Objects.equals(n.getNumber(), number))
                .findFirst()
                .orElse(null);
    }

    public Account updateCustomer(Long id, Customer c) {
        Account one = getOne(id);
        Customer customer = new Customer(c.getName(), c.getEmail(), c.getAge());
        one.setCustomer(customer);
        return save(one);
    }

    public List<Account> updateCustomerData(long cId, Customer c) {
        return data.stream().filter(a -> a.getCustomer().getId() == cId).toList();
    }
}