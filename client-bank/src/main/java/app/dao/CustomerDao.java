package app.dao;

import app.entity.Account;
import app.entity.Currency;
import app.entity.Customer;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerDao implements DAO<Customer> {
    private final List<Customer> data = new ArrayList<>();
    private static long counter = 1;

    public CustomerDao() {
        fill();
    }

    private void fill(){
        Customer elena = new Customer("Elena", "elena@gmail.com", 27);
        elena.setId(1L);
        Customer max = new Customer( "Max", "max@gmail.com", 55);
        max.setId(2L);
        Customer gim = new Customer("Gim", "gim@gmail.com", 38);
        gim.setId(3L);
        Account elenaA = new Account(1L,"f2962718-8cf6-5040-b44d-f62c042f5d41",Currency.EUR, 4000.00, elena);
        Account elenaA2 = new Account(2L,"96ad01f4-332b-59b9-bb72-131305c5636a", Currency.UAH, 37.80, elena);
        Account maxA = new Account(3L, "8e39f30d-6402-5d5d-85ac-86ee78b423cf",  Currency.USD, 250.00, max);
        Account gimA = new Account(4L,"c359657d-6995-5c3d-b85d-84871637954c", Currency.USD, 700.00, gim);
        save(elena);
        save(max);
        save(gim);

        addAccount(elena.getId(),  elenaA);
        addAccount(elena.getId(),  elenaA2);
        addAccount(max.getId(),  maxA);
        addAccount(gim.getId(),  gimA);
    }

    @Override
    public Customer save(Customer obj) {
        if(!data.contains(obj)) {
            obj.setId(counter++);
            data.add(obj);
        }
        return obj;
    }

    @Override
    public boolean delete(Customer obj) { return data.remove(obj); }

    @Override
    public void deleteAll(List<Customer> entities) { data.removeAll(entities); }

    @Override
    public void saveAll(List<Customer> entities) { data.addAll(entities); }

    @Override
    public List<Customer> findAll() { return data; }

    @Override
    public boolean deleteById(long id) { return data.removeIf(c -> c.getId() == id); }

    @Override
    public Customer getOne(long id) {
        return data
                .stream()
                .filter(c -> c.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public Customer addAccount(Customer c, Account a) {
        a.setCustomer(null);
        List<Account> accounts = c.getAccounts();
        accounts.add(a);
        c.setAccounts(accounts);
        return save(c);
    }

    public Customer addAccount(Long cId, Account a) {
        Customer c = getOne(cId);
        a.setCustomer(null);
        c.addAccount(a);
        return save(c);
    }

    public void updateAccount(Long cId, Account a) {
        Customer c = getOne(cId);
        c.updateAccount(a);
        save(c);
    }

    public void deleteAccount(Long cId, Account a) {
        Customer c = getOne(cId);
        a.setCustomer(null);
        c.deleteAccount(a);
        save(c);
    }
}
