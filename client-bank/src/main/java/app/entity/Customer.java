package app.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.LinkedList;
import java.util.List;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class Customer {
    private Long id;
    private String name;
    private String email;
    private Integer age;
    private List<Account> accounts = new LinkedList<>();

    public Customer(String name, String email, Integer age) {
        this.name = name;
        this.email = email;
        this.age = age;
    }

    public void addAccount(Account a){
        accounts.add(a);
    }

    public void updateAccount(Account a){
        accounts = accounts.stream().map(x -> {
            if (x.getId().equals(a.getId())) return a;
            return x;
        }).toList();
    }

    public void deleteAccount(Account a){
        accounts.remove(a);
    }
}