package app.service;

import app.dao.CustomerDao;
import app.entity.Account;
import app.entity.Customer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerDao dao;

    public Customer saveCustomer(Customer c) { return dao.save(c); }

    public Customer updateCustomer(long id, Customer updatedCustomer) {
        Customer customer = getCustomerById(id);
        if (customer != null) {
            customer.setName(updatedCustomer.getName());
            customer.setAge(updatedCustomer.getAge());
            customer.setEmail(updatedCustomer.getEmail());
            return customer;
        }
        return dao.save(updatedCustomer);
    }

    public List<Customer> getAllCustomers() { return dao.findAll(); }

    public Customer getCustomerById(Long id) { return  dao.getOne(id); }

    public boolean deleteCustomer(Customer c) { return dao.delete(c); }

    public boolean deleteCustomerById(Long id) { return dao.deleteById(id); }

    public Customer addAccount(Long id, Account a) { return dao.addAccount(id, a); }

    public void updateAccount(Account a){
        dao.updateAccount(a.getCustomer().getId(), a);
    }

    public void deleteAccount(Long id, Account a) { dao.deleteAccount(id, a); }
}