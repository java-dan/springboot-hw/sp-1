package app.service;

import app.dao.AccountDao;
import app.entity.Account;
import app.entity.Currency;
import app.entity.Customer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountService {
    private final AccountDao dao;

    public Account saveAccount(Account acc) {
        return dao.save(acc);
    }

    public Account createAccount(Currency currency, Customer customer) {
        return dao.createAccount(currency, customer);
    }

    public List<Account> getAccounts() {
        return dao.findAll();
    }

    public Account getAccountById(long id){
        return dao.getOne(id);
    }

    public Account getAccountByNumber(String number){
        return dao.findByNumber(number);
    }

    public boolean deleteAccount(long id){
        return dao.deleteById(id);
    }

    public void deleteAll(List<Account> accounts){
        dao.deleteAll(accounts);
    }

    public Account addFunds(String number, Double amount) {
        Account account = getAccountByNumber(number);
        Double balance = account.getBalance();
        account.setBalance(balance + amount);
        saveAccount(account);
        return account;
    }

    public Account transferFrom(String n, double amount){
        Account account = getAccountByNumber(n);
        Double balance = account.getBalance();
        if (balance < amount) throw new ArithmeticException();
        account.setBalance(balance - amount);
        saveAccount(account);
        return account;
    }

    public boolean transfer(String from, String to, double amount){
        Account accountFrom = getAccountByNumber(from);
        Account accountTo = getAccountByNumber(to);
        Double balanceFrom = accountFrom.getBalance();
        if (balanceFrom < amount) return false;
        transferFrom(from, amount);
        addFunds(to, amount);
        return true;
    }

    public Account updateCustomer(Long id, Customer c){
        return dao.updateCustomer(id, c);
    }

    public void updateCustomerData(Long cId, Customer c) {
        List<Account> accounts = dao.updateCustomerData(cId, c);
        accounts.forEach(a -> updateCustomer(a.getId(), c));
    }
}