package app.controller;

import app.entity.Account;
import app.service.AccountService;
import app.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class AccountController {
    private final AccountService accountService;
    private final CustomerService customerService;

    @GetMapping
    public List<Account> getAllAccounts() {
        return accountService.getAccounts();
    }

    @GetMapping("{id}")
    public Account getAccount(@PathVariable("id") Long id) {
        return accountService.getAccountById(id);
    }

    @PutMapping("/deposit/{number}")
    public ResponseEntity<Account> addFunds(@PathVariable("number") String number, @RequestBody Double amount) {
        Account account = accountService.addFunds(number, amount);
        customerService.updateAccount(account);
        return ResponseEntity.ok(account);
    }

    @PutMapping("/withdraw/{number}")
    public ResponseEntity<Account> withdraw(@PathVariable("number") String number, @RequestBody Double amount) {
        try {
            Account account = accountService.transferFrom(number, amount);
            customerService.updateAccount(account);
            return ResponseEntity.ok(account);
        } catch (ArithmeticException e){
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/transfer/{from}/{to}")
    public ResponseEntity<Boolean> transfer(@PathVariable("from") String from, @PathVariable("to") String to, @RequestBody Double amount) {
        Account accountFrom = accountService.getAccountByNumber(from);
        Account accountTo = accountService.getAccountByNumber(to);
        boolean b = accountService.transfer(from, to, amount);
        if (b){
            customerService.updateAccount(accountFrom);
            customerService.updateAccount(accountTo);
            return ResponseEntity.ok(b);
        } else {
            return ResponseEntity.badRequest().build();
        }
    }
}