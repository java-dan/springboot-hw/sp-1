package app.controller;

import app.entity.Account;
import app.entity.Currency;
import app.entity.Customer;
import app.service.AccountService;
import app.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customers")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class CustomerController {
    private final CustomerService customerService;
    private final AccountService accountService;

    @GetMapping
    public List<Customer> getAllCustomers(){
        return  customerService.getAllCustomers();

    }
    @GetMapping("/{id}")
    public Customer getCustomer(@PathVariable("id") Long id){
        return customerService.getCustomerById(id);
    }

    @PostMapping
    public Customer createCustomer(@RequestBody Customer c) {
        System.out.println(c);
        return customerService.saveCustomer(c);
    }

    @PutMapping("/{id}")
    public Customer updateCustomer(@PathVariable("id") Long id, @RequestBody Customer c) {
        Customer customer = customerService.updateCustomer(id, c);
        accountService.updateCustomerData(id, customer);
        return customer;
    }

    @DeleteMapping("/{id}")
    public boolean deleteCustomer(@PathVariable("id") Long id) {
        List<Account> accounts = customerService.getCustomerById(id).getAccounts();
        accountService.deleteAll(accounts);
        return customerService.deleteCustomerById(id);
    }

    @PostMapping("/{id}/accounts")
    public Customer createAccount(@PathVariable("id") Long id, @RequestBody Currency currency) {
        Customer customerById = customerService.getCustomerById(id);
        Account account = accountService.createAccount(currency, customerById);
        Customer customer = customerService.addAccount(id, account);
        accountService.updateCustomer(account.getId(), customer);
        return customer;
    }

    @DeleteMapping("/{id}/accounts/{accountId}")
    public void deleteAccount(@PathVariable("id") Long id, @PathVariable("accountId") Long accId) {
        Account accountById = accountService.getAccountById(accId);
        customerService.deleteAccount(id, accountById);
        accountService.deleteAccount(accId);
    }
}
