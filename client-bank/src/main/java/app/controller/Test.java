package app.controller;

import app.entity.Account;
import app.entity.Currency;
import app.entity.Customer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Test {
    @RequestMapping("check")
    public String check() {
        Customer c = new Customer("ALex", "alex@mail.com", 19);
        Account acc = new Account(Currency.USD, c);
        System.out.println(acc);
        return acc.toString();
    }

    @RequestMapping("/")
    public String home(){
        return "Hello World!";
    }
}
